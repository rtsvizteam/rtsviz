jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
} 

//http://stackoverflow.com/questions/5302037/javascript-string-replace-lt-into										
convert = function(convert){
	return $("<span />", { html: convert }).text();
};


//http://api.jquery.com/category/selectors/
//$('input[id*="_date"]')
//$('input[id$="_date"]')
//$('textarea[id$="_temp"]')

updateValues = function () {	
	$('textarea[id$="_temp"]').each(function() {
		var sourceId = $(this).attr("id");
		var targetId = sourceId.replace("_temp","");
		var sourceValue = $('#' + sourceId).val().replace(/(\r\n|\n|\r)/gm,",");
		// copy value
		$('#' + targetId).val(sourceValue).change();		
	});

	$('input[id$="_temp"][type="text"],[type="number"]').each(function() {	
		var sourceId = $(this).attr("id");
		var targetId = sourceId.replace("_temp","");
		var sourceValue = $('#' + sourceId).val().replace(/(\r\n|\n|\r)/gm,",");
		// copy value
		$('#' + targetId).val(sourceValue).change();		
	});

	
	$('input[id$="_temp"][type="file"]').each(function() {	
		var sourceId = $(this).attr("id");
		var targetId = sourceId.replace("_temp","");
		var sourceValue = $('#' + sourceId).parent().parent().next().val().replace(/(\r\n|\n|\r)/gm,",");
		// copy value
		$('#' + targetId).val(sourceValue).change();		
	});

	
	$('input[id$="_temp"][type="password"]').each(function() {	
		var sourceId = $(this).attr("id");
		var targetId = sourceId.replace("_temp","");
		var sourceValue = $('#' + sourceId).val().replace(/(\r\n|\n|\r)/gm,",");
		// copy value
		$('#' + targetId).val(sourceValue).change();		
	});
	
			
	$('input[id$="_temp"][type="checkbox"]').each(function() {	
		var sourceId = $(this).attr("id");
		var targetId = sourceId.replace("_temp","");
		var sourceValue = $('#' + sourceId).is(':checked');
		// copy value
		$('#' + targetId).val(sourceValue).change();		
	});	

	
	$('input[id$="_temp"][type="radio"]').each(function() {	
		var sourceId = $(this).attr("id");
		var targetId = sourceId.replace("_temp","");
		var sourceValue = $('#' + sourceId).is(':checked');
		// copy value
		$('#' + targetId).val(sourceValue).change();		
	});	

	
	$('select[id$="_temp"]').each(function() {
		var sourceId = $(this).attr("id");
		var targetId = sourceId.replace("_temp","");
		//var sourceValue = $(this).find("option:selected").map(function(){ return this.value }).get().join(",");
		var sourceValue = $(this).find("option:selected").map(function(){ return this.value }).get().join(";");
		// copy value
		$('#' + targetId).val(sourceValue).change();		
	});

	$('div[id$="_temp"][class^="shiny-date-range-input"]').each(function() {
		var sourceId = $(this).attr("id");
		var targetId = sourceId.replace("_temp","");		
		var sourceValue = $(this).find("input").map(function(){ return this.value }).get().join("::");
		
		// copy value
		$('#' + targetId).val(sourceValue).change();		
	});

	$('div[id$="_temp"][class^="shiny-date-input"]').each(function() {
		var sourceId = $(this).attr("id");
		var targetId = sourceId.replace("_temp","");		
		var sourceValue = $(this).find("input").map(function(){ return this.value }).get().join("::");
		
		// copy value
		$('#' + targetId).val(sourceValue).change();		
	});
	
}

//not using this
//http://stackoverflow.com/questions/2596387/how-to-use-trigger-in-jquery-to-pass-parameter-data-as-well-as-control-this
//http://forum.jquery.com/topic/firing-an-event-on-addclass
jQuery(function($){
	$.each(["addClass","removeClass"],function(i,methodname){
	      var oldmethod = $.fn[methodname];
	      $.fn[methodname] = function(){
	            oldmethod.apply( this, arguments );
	            this.trigger(methodname+"change", arguments);
	            return this;
	      }
	});
});
	
addChevronHandle = function () {	
					$(".collapse-group .icon-chevron-up, .icon-chevron-down").on("click", function(e) {
    					e.preventDefault();
    					var $this = $(this);
    					var $collapse = $this.closest(".collapse-group").find(".collapse");
    					$collapse.collapse("toggle");
					});
					
					$(".icon-chevron-up, .icon-chevron-down").click(function(){
    					$(this).toggleClass("icon-chevron-up icon-chevron-down");
					});					
}





//http://bugs.jquery.com/ticket/5858

//http://stackoverflow.com/questions/11816384/how-to-prevent-ff-close-websocket-connection-on-esc			
$(window).keydown(function(event) {
    // check for escape key
    if (event.which == 27) {
        // the following seems to fix the symptom but only in case the document has the focus
        event.preventDefault();
    }
});			



// based on unbindInputs
listShinyBounds = function(){		
	var summary = "Inputs:" + " \n ";
    var inputs = $(document).find('.shiny-bound-input');
    for (var i = 0; i < inputs.length; i++) {
		if ($(inputs[i]).attr("id").slice(-5) != "_temp" &&
			$(inputs[i]).attr("id").substring(0, 5) != "remv_" &&
			$(inputs[i]).attr("id").substring(0, 5) != "hidn_"
		)
		summary = summary + $(inputs[i]).attr("id") + " = " + $(inputs[i]).val() + " \n ";
	}
	summary = summary + " \n " + "Outputs:" + " \n ";
    var outputs = $(document).find('.shiny-bound-output');
    for (var i = 0; i < outputs.length; i++) {
		if ($(outputs[i]).attr("id").slice(-5) != "_temp" &&
			$(outputs[i]).attr("id").substring(0, 5) != "remv_" &&
			$(outputs[i]).attr("id").substring(0, 5) != "hidn_"
		)	    
		summary = summary + $(outputs[i]).attr("id") + " = " + $(outputs[i]).val() + " \n ";
	}	
	return summary;
}



// Login functionality
function sendLogin(){	
	updateValues();

	//http://caligatio.github.io/jsSHA/	
	var shaObj = new jsSHA("SHA-512", "TEXT",1);
	shaObj.update($("#username").val() + $("#password").val());	
	var hash = shaObj.getHash("HEX");
	
	shaObj = new jsSHA("SHA-512", "TEXT",1);
	shaObj.update(makeRandomString(512));	
	var cnonce = shaObj.getHash("HEX");
	
	shaObj = new jsSHA("SHA-512", "TEXT",1);
	shaObj.update(hash + ":" + cnonce + ":" + $("#remv_nonce").val());	
	hash = shaObj.getHash("HEX");
	
	$("#remv_cnonce").val(cnonce).change();
	$("#remv_hash").val(hash).change();
}


function makeRandomString(bits) {
    bytes = Math.ceil(bits / 8);
    str = '';
    for (i = 0; i < bytes; i++) {
        str = str + String.fromCharCode(mt_rand(0, 255));
    }
    return str;
}

function mt_rand (min, max) {
  // http://kevin.vanzonneveld.net
  // +   original by: Onno Marsman
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +   input by: Kongo
  // *     example 1: mt_rand(1, 1);
  // *     returns 1: 1
  var argc = arguments.length;
  if (argc === 0) {
    min = 0;
    max = 2147483647;
  }
  else if (argc === 1) {
    throw new Error('Warning: mt_rand() expects exactly 2 parameters, 1 given');
  }
  else {
    min = parseInt(min, 10);
    max = parseInt(max, 10);
  }
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
