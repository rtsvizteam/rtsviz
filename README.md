rtsviz
====


Interactive Visualization for R Time Series data.
===


The `rtsviz` package creates an interactive
graphical interface to visualize time series data.
The package contains following **visualization** tools(plugins):

* Chart tool - plot time series data and generate summary performance table
* Seasonality tool - visualize historical seasonal patterns

The package can easily be extended with additional plugins.



Installation:
===

```R
remotes::install_bitbucket('rtsvizteam/rtsviz')
```
	
The [CRAN](https://cran.r-project.org) version coming soon.


Example : 
===

To run this example, please install with remotes:

```R
remotes::install_bitbucket('rtsvizteam/rtsviz')
```


```R
	library(quantmod)
	
	# enable persistent time series data storage with `rtsdata` package
	library(rtsdata)

	# load `rtsviz` package
	library(rtsviz)

	
	# start Shiny interface
	launchApp()
	
	
	# start Shiny interface with Authentication
	#  the default authentication include 
	#  demo/demo and test/test username/password settings
	launchApp(include.login = TRUE)
	
	
	# start 'Chart' app
	launchApp('chart')
	
	
	# start 'Chart' app with Authentication
	launchApp('chart', include.login = TRUE)
	
	
	# Test tickers:
	# SPY / Yahoo
	# DTB3 / FRED
	# CAT / Yahoo - works well in Nov
	
```	


The default authentication settings included with `rtsviz` are only for
demonstration purposes. Please create your own **private** authentication
settings if you plan to use authentication functionality.

For example, you can store the authentication settings in file.
Instruct `rtsviz` your own **private** authentication settings with following code:

```R
	# load `auth.data`, authentication settings, from file
	load(file='auth.data.Rdata')
	rtsviz::rtsviz.options.set(login.auth.data = auth.data)
```

Create your own **private** authentication settings with following code:

```R
	auth.data = list(
		'demo' = 'demo'
		,'test' = 'test'
	)

	# only keeps hashes of passwords
	for(i in  names(auth.data))
		auth.data[[i]] = digest::digest(paste0(i, auth.data[[i]]), algo='sha512',serialize=F)
	
	# save `auth.data`, authentication settings, to file
	save(auth.data, file='auth.data.Rdata')
```


The plugin system is easily extendable with your own examples. For example, let's add the
[Welcome to Shiny first example](https://shiny.rstudio.com/tutorial/written-tutorial/lesson1/)
to the `rtsviz` application. First define the ui and server functionality.

```R
	# [Welcome to Shiny first example](https://shiny.rstudio.com/tutorial/written-tutorial/lesson1/)
	library(shiny)

	# Define UI for app that draws a histogram ----
	example.ui <- fluidPage(
		titlePanel("Hello Shiny!"),
	  
		sidebarLayout(  
			sidebarPanel(
				sliderInput(inputId = "bins",
					label = "Number of bins:",
					min = 1,
					max = 50,
					value = 30)
			),

			mainPanel(
				plotOutput(outputId = "distPlot")
			)
		)
	)

	# Define server logic required to draw a histogram ----
	example.server <- function(input, output) {
		# Histogram of the Old Faithful Geyser Data ----
		# with requested number of bins
		output$distPlot <- renderPlot({
			x    <- faithful$waiting
			bins <- seq(min(x), max(x), length.out = input$bins + 1)

			hist(x, breaks = bins, col = "#75AADB", border = "white",
				xlab = "Waiting time to next eruption (in mins)",
				main = "Histogram of waiting times")
		})
	}

	# Create Shiny app ----
	# shinyApp(ui = ui, server = server) 
```

Next register the example.ui and example.server with `rtsviz` application.

```R
	library(quantmod)
	
	# enable persistent time series data storage with `rtsdata` package
	library(rtsdata)

	# load `rtsviz` package
	library(rtsviz)

	# register the example.ui and example.server with `rtsviz` application.
	register.rtsviz.plugin('shiny.example1',
		'Shiny Example1','Shiny Example Lesson1',
		'Histogram of the Old Faithful Geyser Data',
		'Examples', example.ui, example.server)
	
	# start Shiny interface
	launchApp()
```
