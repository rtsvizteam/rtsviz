% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/seasonality.helpers.R
\name{seasonality.technical.data}
\alias{seasonality.technical.data}
\title{Generate Technical Indicators for charts for Seasonality shiny app}
\usage{
seasonality.technical.data(data, ma1.len = 20, ma2.len = 50)
}
\arguments{
\item{data}{xts object with historical time series data}

\item{ma1.len}{moving average look back}

\item{ma2.len}{moving average look back}
}
\value{
list of xts objects with technical indicators
}
\description{
Generate Technical Indicators for charts for Seasonality shiny app
}
