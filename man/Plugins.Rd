% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/rtsviz.r
\name{register.rtsviz.plugin}
\alias{register.rtsviz.plugin}
\alias{rtsviz.plugins}
\title{Plugins}
\usage{
register.rtsviz.plugin(id, short.name, name, description, group, ui,
  server, overwrite = TRUE)

rtsviz.plugins()
}
\arguments{
\item{id}{unique id for plugin; id is used to identify plugin}

\item{short.name}{short name for plugin; short name is used as label in the navigation bar}

\item{name}{name for plugin; name is used as title in the about ui}

\item{description}{description of plugin; description is used as description in the about ui}

\item{group}{navigation menu group(s) to place this plugin}

\item{ui}{shiny ui object}

\item{server}{shiny server object}

\item{overwrite}{flag to overwrite data source if already registered in the list of plugins, \strong{defaults to TRUE}}
}
\value{
None
}
\description{
List available plugins and Register new ones
}
