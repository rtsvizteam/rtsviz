###############################################################################
#' Main shiny app Server object
#'
#' Create the 'main' shiny Server
#'
#' @param input list of input objects for shiny app
#' @param output list of output objects for shiny app
#' @param session session for shiny app
#'
#' @return shiny Server object
#'
#' @export
###############################################################################
main.server = function(input, output, session) {
	#--------------------------
	# define variables
	#--------------------------
	data_env = new.env()
		data_env$shinyapp = session
		data_env$nonce = rtsviz.options()$login.nonce
		data_env$login.check.authentication = rtsviz.options()$login.check.authentication
		
		data_env$home.item = rtsviz.options()$home.item
		data_env$highlight.item = rtsviz.options()$highlight.item
	
		data_env$include.login = rtsviz.options()$include.login
		data_env$include.navbar = rtsviz.options()$include.navbar

	
	
	
	# cache
	cache = list()
	
	# use reactive values so that observe will fire
all_env = reactiveValues(validUser = (!data_env$include.login), toolID.ui = '', toolID.server = '')

	# debug
	debug = ifnull(getOption('RTSVIZ_DEBUG'), FALSE)

	#--------------------------
	# main output
	#--------------------------	
	output$dynamicScreen = renderUI({
		# record UI ID
		tool.id = all_env$toolID.ui
		all_env$toolID.server = all_env$toolID.ui # trigger server
		
if(debug) cat('\n\n\t UI ==================', tool.id, '=== \n\n');

		if(all_env$validUser == TRUE) {
			result = app.ui(tool.id)
		} else {
			result = run.ui( rtsviz.plugins()[[ 'login' ]]$ui )
		}
			
    	if (is.null(result) || length(result) == 0)
        	NULL
		else
			result
	})

	# helper function to handle both function and expression
	run.ui = function(ui) iif(is.function(ui), ui(), ui)
	
	#--------------------------
	# UI logic
	#--------------------------	
	app.ui = function(tool.id) {
		# set default value for nav bar to tool.id
		default.algo = ''
		if(tool.id != '') default.algo = tool.id

		# get navbar
		if(data_env$include.navbar) {
			if( is.null(cache$navbar) ) cache$navbar = app.ui.cache()
		
			# update selected tool
			navbar = gsub('data-nav-value=";tool;"'
						,paste0('data-nav-value="', default.algo, '"')
						,as.character(cache$navbar)
					)
		}
		
		# return UI
		if( is.null(cache$ui) ) cache$ui = list()
		if( is.null(cache$ui$default) ) cache$ui$default = run.ui( rtsviz.plugins()[[ data_env$home.item ]]$ui )
		if(tool.id != '')
			if( is.null(cache$ui[[tool.id]]) ) 
				cache$ui[[tool.id]] = run.ui( rtsviz.plugins()[[tool.id]]$ui )

		tagList(
			iif(data_env$include.navbar, HTML(navbar), NULL)
			,iif(tool.id == '', cache$ui$default, cache$ui[[tool.id]])
		)		
	}

	app.ui.cache = function() {
		default.algo = ";tool;"
		
		# generate list of plugins
		plugins = rtsviz.plugins()
		map = list()
		for(i in 1:len(plugins)) {
			plugin = plugins[[i]]
			group = plugin$group[1]
			
			# skip any plugins with empty groups
			if(nchar(group) == 0) next
			
			temp = ifnull(map[[ group ]], list())
			temp[[ plugin$short.name ]] = plugin$id
			map[[ group ]] = temp
		}
		
		
		# generate list for navbar
		tools = c(
			map
			,make.list('Help', list(
					'About' = 'about' 
					,'Info'='info'
				)
			)
		)
		
		
		# control help menu 
		help.flag = debug			
		if(!help.flag) tools$Help=NULL
		
		
		# define navbar, id='tool'
		ihome = data_env$home.item
		ihigh = data_env$highlight.item
				
		navbar = shiny.make.menu(id='tool'
			,tools
			,default.algo
			,home.item = list(label = plugins[[ihome]]$short.name, tool = ihome)
			,highlight.item = list(label = plugins[[ihigh]]$short.name, tool = ihigh)
		)

		# Inject menu code to bind 'info' to alert(listShinyBounds())
		as.character(tagList(
			iif(help.flag,
				tags$script('   
		        $(function() {
					$(".navbar").bind("click", function(event) {						
						var target = $( event.target );
						
						// start debugger
						// debugger;
						
						// logic to show info
						if(!target.hasClass("dropdown-toggle"))
							if($(this).data("nav-value") == "info") 
								alert(listShinyBounds());
					});	
		        });'
				, type = 'text/javascript')
			,NULL)
				
			,navbar
  		))
	}

	
	#--------------------------
	# Authentication logic
	#--------------------------		
	observe({	
		if(!is.null(input$username) & !is.null(input$remv_hash))		
			if(input$username != "" & input$remv_hash != "") {		

				flag = data_env$login.check.authentication(input$username
					,input$remv_hash
					,data_env$nonce
					,input$remv_cnonce			
				)
				
				if(flag) all_env$validUser = T
			}
	})
	
	
	#--------------------------
	# NavBAR logic - change UI tool.id
	# input$tool is bound to navbar
	#--------------------------	
	observe({
		if( !is.null(input$tool) )
			if( input$tool != '' )
				if( input$tool != 'info' ) # special case
					if( input$tool != all_env$toolID.ui ) 
						all_env$toolID.ui = input$tool			
	})

	
	#--------------------------
	# change Server tool.id
	#--------------------------	
	observe({
		if(all_env$validUser == T) {
			tool.id = all_env$toolID.server		
			app.server(input, output, tool.id)
		} else {
			run.server( rtsviz.plugins()[[ 'login' ]]$server, input, output, session )
		}
	})

	# helper function to handle 2 or 3 function arguments 
	run.server = function(server, input, output, session) iif(length(formals(server)) == 3, server(input, output, session), server(input, output))	
	
	
	#--------------------------
	# Server logic - load proper server
	#--------------------------		
	app.server = function(input, output, tool.id) {
		default.server = rtsviz.plugins()[[ data_env$home.item ]]$server

		
if(debug) cat('\n\n\t Server ==================', tool.id, '=== \n\n');
		
		if(tool.id == '')
			run.server( default.server, input, output, session )
		else
			run.server( rtsviz.plugins()[[tool.id]]$server, input, output, session )
	}

}
