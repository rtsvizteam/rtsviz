.onLoad <- function(libname, pkgname) {
	# register rtsviz plugins, do not overwrite if already registered
	register.rtsviz.plugin('seasonality', 
		'Seasonality','Seasonality Tool',
		'Interactively Explore Seasonality Patterns',
		'Tools', seasonality.ui, seasonality.server,
		overwrite=FALSE)

	register.rtsviz.plugin('seasonality.batch', 
		'Seasonality Batch','Seasonality Batch Tool',
		'Batch to Export Seasonality Results',
		'Tools', seasonality.batch.ui, seasonality.batch.server,
		overwrite=FALSE)
		
	register.rtsviz.plugin('chart', 
		'Chart','Chart Tool',
		'Create Stock Chart and Summary Table',
		'Tools', chart.ui, chart.server,
		overwrite=FALSE)
		
	register.rtsviz.plugin('about', 
		'About','About Tool',
		'Present all registered plugins',
		'', about.ui, about.server,
		overwrite=FALSE)

	register.rtsviz.plugin('login', 
		'Login','Login Tool',
		'Authenticate users',
		'', login.ui, login.server,
		overwrite=FALSE)
	
		
	register.options(overwrite=FALSE)
	
	rtsviz.options.set(
		login.auth.data = sample.auth.data()
		,login.check.authentication = login.check.authentication
		,overwrite=FALSE)
		
		
	invisible()
}



