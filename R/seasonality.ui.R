###############################################################################
#' Seasonality shiny app UI object
#'
#' Create the 'seasonality' shiny application user interface
#' @return shiny UI object
#'
#' @export
###############################################################################
seasonality.ui = function() {
	pageWithSidebar(
	
		headerPanel("")
		
		#--------------------------
		# sidebar
		#--------------------------	
		,sidebarPanel(
			mod_appID_UI("seasonality.applicationID")

			,mod_load.resources_UI()
						
			,mod_load.data_UI('inputSymbol')
			
			,wellPanel(id='inputPanel2', style="max-width:100%;"
				,addTooltip(
					tempSelectInput(
						"dataMonth"
						,"Month"
						,choices =  spl('Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec')
						,selected='Jan'
					)
					,"Select month."
				)

				,customButton(
					"Run Monthly"
					,"updateValues();"
					,class="btn-success"
					,"mainRun2"
				)								
			)
			
			# run panel
			,create.run.panel(run.button=FALSE)
			
		)
		
		# [Change the color of action button in shiny](https://stackoverflow.com/questions/33620133/change-the-color-of-action-button-in-shiny)
		# [Background color of tabs in shiny tabPanel](https://stackoverflow.com/questions/35025145/background-color-of-tabs-in-shiny-tabpanel)
		# [Create a Link to the Top of a Web Page Without Using Anchors](https://stackoverflow.com/questions/11750862/create-a-link-to-the-top-of-a-web-page-without-using-anchors)
		# [Need an unordered list without any bullets](https://stackoverflow.com/questions/1027354/need-an-unordered-list-without-any-bullets)
		# [Bootstrap buttons](https://getbootstrap.com/docs/4.0/components/buttons/)
		#--------------------------
		,mainPanel(
			tags$style(HTML("
			.tabbable > .nav > li > a[data-value='Summary'] {color: #fff;background-color: #17a2b8;border-color: #17a2b8;}
			.tabbable > .nav > li > a[data-value='Details'] {color: #fff;background-color: #138496;border-color: #117a8b;}
			.tabbable > .nav > li > a[data-value='Stat'] {color: #fff;background-color: #dc3545;border-color: #dc3545;}			
			.tabbable > .nav > li > a[data-value='Month'] {color: #fff;background-color: #28a745;border-color: #28a745;}
			.tabbable > .nav > li > a[data-value='Month details'] {color: #fff;background-color: #218838;border-color: #1e7e34;}
			.tabbable > .nav > li > a[data-value='Help'] {color: #fff;background-color: #6c757d;border-color: #6c757d;}
			"
			)),
						
			# [https://pixelflips.com/blog/anchor-links-with-a-fixed-header](https://pixelflips.com/blog/anchor-links-with-a-fixed-header)
			tags$style(HTML("
			.anchor{
				display: block;
				position: relative;
				top: -35px;
				visibility: hidden;
			}")),
			
			tabsetPanel(
				tabPanel("Summary"
					#,uiOutput('appFirstHelp')
					,mod_first.help_UI("firstHelp")
				
					,tags$h3('Performance Summary:')
					,tags$ul(class="list-unstyled"
						,tags$li(tags$a(href='#summaryB1','Historical Monthly Performance'))
						,tags$li(tags$a(href='#summaryB2','Historical Annual Performance'))
						,tags$li(tags$a(href='#summaryB3','Overall Performance'))
						,tags$li(tags$a(href='#summaryB4','Last 6 Months Performance'))
						,tags$li(tags$a(href='#summaryB5','Download Reports'))
					)
						
					,tags$br()
					,tags$span(class='anchor', id='summaryB1')
					,tags$h3('Historical Monthly Performance:', tags$a(href="#top", icon('arrow-circle-up')))
					,plotOutput("summaryStatsPlot", height = "400px")
					
					,tags$br()
					,tags$span(class='anchor', id='summaryB2')
					,tags$h3('Historical Annual Performance:', tags$a(href="#top", icon('arrow-circle-up')))
					,plotOutput("summaryAnnualPlot", height = "400px")
					
					,tags$br()
					,tags$span(class='anchor', id='summaryB3')
					,tags$h3('Overall Performance:', tags$a(href="#top", icon('arrow-circle-up')))
					,plotOutput("summaryFullPlot", height = "400px")
					
					,tags$br()
					,tags$span(class='anchor', id='summaryB4')
					,tags$h3('Last 6 Months Performance:', tags$a(href="#top", icon('arrow-circle-up')))
					,plotOutput("summary6MPlot", height = "400px")


					,tags$br()
					,tags$span(class='anchor', id='summaryB5')
					,tags$h3('Download Reports:', tags$a(href="#top", icon('arrow-circle-up')))					
					# download CSV / PDF / Rmd
					,downloadButton("downloadRmd", "Rmd")
					,downloadButton("downloadReport", "PDF")
					,downloadButton("downloadData", "CSV")
					
					,tags$br()
					,tags$br()
				),
				
				tabPanel("Details"
					,tags$h3('Performance Details:')
					,tags$ul(class="list-unstyled"
						,tags$li(tags$a(href='#detailsB1','Historical Monthly Table'))
						,tags$li(tags$a(href='#detailsB2','Historical Performance Table'))
					)
						
					,tags$br()
					,tags$span(class='anchor', id='detailsB1')
					,tags$h3('Historical Monthly Table:', tags$a(href="#top", icon('arrow-circle-up')))
					,uiOutput("summaryStatsTable")
					,tags$h3(
						uiOutput("summaryStatsTableCopy", TRUE)
						,tags$a(href="#top", icon('arrow-circle-up'))
					)						
#[good example copy to cliboard](https://getbootstrap.com/docs/4.0/components/buttons/)

# Todo
# add [Testing for Seasonality In Excel](https://quantmacro.wordpress.com/2015/06/29/testing-for-seasonality-in-excel/)
					
					,tags$br()
					,tags$span(class='anchor', id='detailsB2')
					,tags$h3('Historical Performance Table:', tags$a(href="#top", icon('arrow-circle-up')))					
					,uiOutput("summaryMonthlyTable")
					,tags$h3(
						uiOutput("summaryMonthlyTableCopy", TRUE)
						,tags$a(href="#top", icon('arrow-circle-up'))
					)
					
					,tags$br()
					,tags$br()
				),

				
				mod_stat.tab_UI("statPanel"),
#				tabPanel("Stat"
#					,tags$h3('Regression Details:')
#					,tags$ul(class="list-unstyled"
#						,tags$li(tags$a(href='#statB1','Regression Coefficients Plot'))
#						,tags$li(tags$a(href='#statB2','Regression Coefficients Table'))
#					)
#										
#					,tags$br()
#					,tags$span(class='anchor', id='statB1')
#					,tags$h3('Regression Coefficients Plot:', tags$a(href="#top", icon('arrow-circle-up')))					
#					,plotOutput("statPlot", height = "400px")					
#										
#					,tags$br()
#					,tags$span(class='anchor', id='statB2')
#					,tags$h3('Regression Coefficients Table:', tags$a(href="#top", icon('arrow-circle-up')))					
#					,uiOutput("statModelTable")
#					,tags$h3(
#						uiOutput("statModelTableCopy", TRUE)
#						,tags$a(href="#top", icon('arrow-circle-up'))
#					)
#					
#					#,uiOutput("statDistTable")
#				),
				
				tabPanel("Month"				
					,tags$h3(uiOutput('monthName1',TRUE), ' Performance Summary:')
					,tags$ul(class="list-unstyled"
						,tags$li(tags$a(href='#msummaryB1',uiOutput('monthName2',TRUE), ' Historical Annual Performance'))
						,tags$li(tags$a(href='#msummaryB2',uiOutput('monthName3',TRUE), ' Daily Historical Performance'))
					)
						
					,tags$br()
					,tags$span(class='anchor', id='msummaryB1')
					,tags$h3(uiOutput('monthName4',TRUE), ' Historical Annual Performance:', tags$a(href="#top", icon('arrow-circle-up')))					
					,plotOutput("detailsMainPlot", height = "400px")
					
					,tags$br()
					,tags$span(class='anchor', id='msummaryB2')
					,tags$h3(uiOutput('monthName5',TRUE), ' Daily Historical Performance:', tags$a(href="#top", icon('arrow-circle-up')))															
					,plotOutput("detailsMonthOverlayPlot", height = "400px")
				),
				
				tabPanel("Month details"
					,tags$h3(uiOutput('monthName6',TRUE), ' Annual Charts:')
					,uiOutput('detailsMonthCharts')
				),
				
				tabPanel("Help"
					,uiOutput('helpDetails')
				)
			)
			
			
			# add copy table in tooltip for table
		)
	)
}
