#' @title `rtsviz` - R Time Series Visualization.
#' 
#' @description This user friendly shiny application allows to interactively explore time series 
#'	and seasonality patterns. It computes summary statistics for each month, creates detailed 
#'	time series plots, and allows user to export the graphs and reports in .pdf, .csv and .Rmd 
#'	formats.
#'
#' 
#' @examples
#' \dontrun{ 
#'	library(quantmod)
#'	library(rtsdata)
#'
#'	# load `rtsviz` package
#'	library(rtsviz)
#'
#'	# start Shiny interface
#'	launchApp()
#'	
#'	# Test tickers:
#'	# SPY / Yahoo
#'	# DTB3 / FRED
#'
#' }
#' 
#' 
#' 
#' @import shiny
#' @importFrom graphics abline axis barplot layout lines par plot
#'
#' @name rtsviz
#' @docType package
#' 
NULL


## quiets concerns of R CMD check
# [no visible binding for global variable](https://github.com/STAT545-UBC/Discussion/issues/451)
# utils::globalVariables(c('valid','app.id','input','session', 'getLogs', 'getStocks', 'getSymbols', 'getData', 'getUserData'), add=FALSE)


###############################################################################
#' Plugins
#'
#' List available plugins and Register new ones
#'
#' @param id unique id for plugin; id is used to identify plugin
#' @param short.name short name for plugin; short name is used as label in the navigation bar
#' @param name name for plugin; name is used as title in the about ui
#' @param description description of plugin; description is used as description in the about ui
#' @param group navigation menu group(s) to place this plugin
#' @param ui shiny ui object
#' @param server shiny server object
#' @param overwrite flag to overwrite data source if already registered in the list of plugins, \strong{defaults to TRUE}
#'
#' @return None
#'
#' @export
#' @rdname Plugins
###############################################################################
register.rtsviz.plugin = function
(
	id,
	short.name,
	name,
	description,
	group,
	ui,
	server,
	overwrite = TRUE
)
	set.options('rtsviz.plugins', make.list(id,
		list(
			id = id,
			short.name = short.name,
			name = name,
			description = description,
			group = group,
			ui = ui,
			server = server
		)
	), overwrite = overwrite)


#' @export
#' @rdname Plugins
rtsviz.plugins = function() ifnull(getOption('rtsviz.plugins'), list())




###############################################################################
#' Package Options
#'
#' Setup Package Options
#'
#' @param home.item home item in the navigation bar, \strong{defaults to 'about'}, only used for 'main' application
#' @param highlight.item item in the navigation bar, \strong{defaults to 'seasonality'}, only used for 'main' application
#' @param ... additional settings
#' @param overwrite flag to overwrite options if already set, \strong{defaults to TRUE}
#'
#' @return None
#'
#' @export
#' @rdname PackageOptions
###############################################################################
register.options = function
(
	home.item = 'about'
	,highlight.item = 'seasonality'
	,overwrite=TRUE
)
	set.options('rtsviz.options', 
		list(
			home.item = home.item,
			highlight.item = highlight.item
		)
		,overwrite=overwrite 
	)


	
#' @export
#' @rdname PackageOptions
rtsviz.options = function() getOption('rtsviz.options')


#' @export
#' @rdname PackageOptions
rtsviz.options.set = function(..., overwrite=TRUE) set.options('rtsviz.options', ..., overwrite=TRUE)
