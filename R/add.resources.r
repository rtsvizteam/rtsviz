###############################################################################
#' Set up 'www' folder path for Shiny app
#'
#' @export
###############################################################################
addResources = function() {	
	# if run in debug mode, locate 'www' folder with resources
	debug = ifnull(getOption('RTSVIZ_DEBUG'), FALSE)
	if(debug) {
		resources = find.resources.folder('rtsviz', '.')
		cat('resources:', resources, '\n')
	} else
		resources = system.file('www', package = 'rtsviz')
	

	
	# add resources folder to shiny's search path
	shiny::addResourcePath('www', resources)
	
	# add resources
	shiny::singleton(
		shiny::tags$head(
			shiny::tags$script(
				src = file.path('www', 'srcjs', 'common.js')
			),
			shiny::tags$script(
				src = file.path('www', 'srcjs', 'navbar.js')
			),
			
			shiny::tags$script(
				src = file.path('www', 'shared', 'jsSHA','sha.js')
			),
			
			shiny::tags$script(
				src = file.path('www', 'shared', 'percentageloader-release-0.1',
					'js', 'jquery.percentageloader-01a.min.js')
			),
			shiny::tags$link(
				rel = 'stylesheet',
				href = file.path('www.rtsviz', 'shared', 'percentageloader-release-0.1',
					'css', 'jquery.percentageloader-01a.css')
			)
			
		)
	)
}



# locate 'www' folder with resources
find.resources.folder = function(pkg, path = '.') {
	# try 5 times
	for(i in 1:5) {
		dirs = list.dirs(path)
		if( length(grep('www$', dirs)) ) break
		path = file.path('..', path)
	}
	
	dirs = dirs[grep('www$', dirs)]
	if(length(dirs))
		dirs = dirs[grep(paste0(pkg,.Platform$file.sep), dirs)]
		
	if(length(dirs))
		resources = dirs[1]
	else
		stop('resources folder not found')
		
	resources
}